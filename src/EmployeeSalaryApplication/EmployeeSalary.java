package EmployeeSalaryApplication;

import java.util.Comparator;

/**
 * Created by Madimetja on 2015/10/11.
 */
public class EmployeeSalary implements Comparator<EmployeeSalary>, Comparable<EmployeeSalary> {
    private String employeeName;
    private String employeeID;
    private Integer employeeSalary;
    private String employeeDepartmentCode;

    EmployeeSalary(String name, String id, Integer salary, String departmentCode) {
        this.employeeName = name;
        this.employeeID = id;
        this.employeeSalary = salary;
        this.employeeDepartmentCode = departmentCode;
    }

    /**
     * Returns the department code of the employee.
     *
     * @return The employee department code.
     */
    public String getDepartmentCode() {
        return this.employeeDepartmentCode;
    }

    @Override
    public String toString() {
        return "Employee Name: " + this.employeeName + "\nEmployee ID: " + this.employeeID +
                "\nEmployee Salary: " + this.employeeSalary + "\nEmployee Department Code: " + this.employeeDepartmentCode;
    }

    public int compareTo(EmployeeSalary obj) {
        return obj.employeeSalary - this.employeeSalary;
    }

    public int compare(EmployeeSalary obj1, EmployeeSalary obj2) {
        return obj2.employeeSalary - obj1.employeeSalary;
    }
}