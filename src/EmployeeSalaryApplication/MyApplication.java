package EmployeeSalaryApplication;

import Exceptions.EmployeeNotFoundException;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class MyApplication {
    private List<EmployeeSalary> salaries;

    public static void main(String[] args) throws Exception {

        MyApplication salaryApplication = new MyApplication();
        salaryApplication.populateEmployeeSalaries();

        System.out.print("Please enter a department code: ");

        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            String departmentCode = br.readLine();

            EmployeeSalary highestPaid = salaryApplication.findHighestPaidEmployee(departmentCode);

            System.out.println("The highest paid employee with department code '" + departmentCode + "' is :\n");
            System.out.println(highestPaid.toString());
        } catch (EmployeeNotFoundException e) {
            System.err.println(e.getMessage());
        } catch (Exception e) {
            System.err.println("Oops, seems something went wrong while finding the highest paid employee.");
        } finally {
            System.out.println("\nApplication terminating.");
        }
    }

    /**
     * Populates the salaries collection with a set of employee salary object.
     */
    private void populateEmployeeSalaries() {
        salaries = new ArrayList<EmployeeSalary>();

        this.salaries.add(new EmployeeSalary("Tyler Bennett", "E10297", 32000, "D101"));
        this.salaries.add(new EmployeeSalary("John Rappl", "E21437", 47000, "D050"));
        this.salaries.add(new EmployeeSalary("George Woltman", "E00127", 53500, "D101"));
        this.salaries.add(new EmployeeSalary("Adam Smith", "E63535", 18000, "D202"));
        this.salaries.add(new EmployeeSalary("Claire Buckman", "E39876", 27800, "D202"));
        this.salaries.add(new EmployeeSalary("David McClellan", "E04242", 41500, "D101"));
        this.salaries.add(new EmployeeSalary("Rich Holcomb", "E01234", 15900, "D101"));
        this.salaries.add(new EmployeeSalary("Nathan Adams", "E41298", 21900, "D050"));
        this.salaries.add(new EmployeeSalary("Richard Potter", "E43128", 15900, "D101"));
        this.salaries.add(new EmployeeSalary("David Motsinger", "E27002", 19250, "D101"));
        this.salaries.add(new EmployeeSalary("Tim Sampair", "E03033", 27000, "D101"));
        this.salaries.add(new EmployeeSalary("Kim Arlich", "E10001", 57000, "D190"));
        this.salaries.add(new EmployeeSalary("Timothy Grove", "E16398", 29900, "D190"));
    }

    /**
     * Finds the highest paid employee from a department. In the case that no employee is found with the given
     * department code, a EmployeeNotFoundException exception will be thrown.
     *
     * @param departmentCode The department to search highest paid employee from.
     * @return The EmployeeSalary object with the highest recorded salary for the given department
     * @throws EmployeeNotFoundException An exception thrown if no employee was found with the given department code
     */
    private EmployeeSalary findHighestPaidEmployee(String departmentCode) throws EmployeeNotFoundException {
        //Sort the collection from highest paid employee to lowest paid employee
        Collections.sort(salaries);

        //Given that the list is sorted, find the first employee with matching department code
        Iterator itr = salaries.iterator();
        while (itr.hasNext()) {
            EmployeeSalary obj = (EmployeeSalary) itr.next();
            if (obj.getDepartmentCode().equals(departmentCode)) {
                return obj;
            }
        }

        //If execution reaches this point, no employee was found with the given department code.
        //Throw an EmployeeNotFound exception.
        throw new EmployeeNotFoundException("No employee was found with the given department code '" + departmentCode + "'.");
    }
}