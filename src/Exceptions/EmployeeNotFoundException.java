package Exceptions;

/**
 * Created by Madimetja on 2015/10/11.
 * Custom Exception class to throw exceptions when an employee object is not found.
 */
public class EmployeeNotFoundException extends Exception {
    public EmployeeNotFoundException(String message) {
        super(message);
    }
}